package ru.pvz.ozon.cofiguration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Getter
@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties
public class ClientConfiguration {

    @Value("${ozon.auth.login}")
    private String login;

    @Value("${ozon.namespace}")
    private String namespace;

    @Value("${ozon.namespaceURI}")
    private String namespaceURI;

    @Value("${ozon.auth.password}")
    private String password;

    @Value("${ozon.auth.contractID}")
    private String contractID;

    @Value("${ozon.server.content_type}")
    private String contentType;

    @Value("${ozon.server.connection_timeout}")
    private int connectionTimeout;

    @Value("${ozon.server.links.CarriageService}")
    private String carriageServiceLink;

    @Value("${ozon.server.soapActions.CarriageListGet}")
    private String soapActionCarriageListGet;

}
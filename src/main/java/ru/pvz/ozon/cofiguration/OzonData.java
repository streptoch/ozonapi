package ru.pvz.ozon.cofiguration;

import java.util.HashMap;
import java.util.Map;

public class OzonData {

    public static final Map<String, String> servicesLink = new HashMap<>();

    static{
        servicesLink.put(OzonServices.CARRIAGE_SERVICE, "http://apitest.ozon-dostavka.ru/subagent/v2/CarriageService.svc");
        servicesLink.put(OzonServices.ARTICLE_SERVICE, "http://apitest.ozon-dostavka.ru/subagent/v2/ArticleService.svc");
        servicesLink.put(OzonServices.INFORMATION_SERVICE, "http://apitest.ozon-dostavka.ru/subagent/v2/InformationService.svc");
        servicesLink.put(OzonServices.MANAGE_SERVICE, "http://apitest.ozon-dostavka.ru/subagent/v2/ManageService.svc");
        servicesLink.put(OzonServices.REPORT_SERVICE, "http://apitest.ozon-dostavka.ru/subagent/v2/ReportService.svc");
    }

    public static String getSoapAction(Object request){

        return  "ozon/api/subagent/I" +  getDirectParent(request) + "/" + request.getClass().getSimpleName();
    }

    public static String getEndpointUrl(Object request){

        return servicesLink.get(getDirectParent(request));
    }

    private static String getDirectParent(Object request){

        String packageName = request.getClass().getPackage().getName();

        String directParent;
        if(packageName.contains(".")) {
            directParent = packageName.substring(1 + packageName.lastIndexOf("."));
        } else {
            directParent = packageName;
        }
        return directParent;
    }

}

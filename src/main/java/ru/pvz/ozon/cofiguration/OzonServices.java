package ru.pvz.ozon.cofiguration;

public final class OzonServices {
    public static final String ARTICLE_SERVICE = "ArticleService";
    public static final String CARRIAGE_SERVICE = "CarriageService";
    public static final String INFORMATION_SERVICE = "InformationService";
    public static final String MANAGE_SERVICE = "ManageService";
    public static final String REPORT_SERVICE = "ReportService";
}

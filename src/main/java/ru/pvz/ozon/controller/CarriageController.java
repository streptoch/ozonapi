package ru.pvz.ozon.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.pvz.ozon.ReportService.PostingProcessedListGet;
import ru.pvz.ozon.cofiguration.ClientConfiguration;
import ru.pvz.ozon.CarriageService.*;
import ru.pvz.ozon.cofiguration.OzonData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import java.io.*;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@RestController()
@AllArgsConstructor
public class CarriageController {

    private final ClientConfiguration config;

    private SoapService soapService;

    @GetMapping("/test")
    public String test(){

        QName qName = new QName(config.getNamespaceURI(),"ContractID");
        JAXBElement<Long> contractID= new JAXBElement<Long>(qName, Long.class, Long.parseLong(config.getContractID()));


        RequestCarriageListGet test = new RequestCarriageListGet();
        test.setLogin(config.getLogin());
        test.setPassword(config.getPassword());
        test.setContractID(contractID);

        test.setStartDate("01.01.2020");
        test.setStopDate("30.01.2020");
        test.setPageSize(100);
        test.setPageNumber(1);
        test.setState("All");

        CarriageListGet request = new CarriageListGet();
        request.setRequest(test);

        soapService.callSoapWebService(request);

        return "/test";
    }

}

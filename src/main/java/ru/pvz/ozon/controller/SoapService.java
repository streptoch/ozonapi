package ru.pvz.ozon.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pvz.ozon.cofiguration.ClientConfiguration;
import ru.pvz.ozon.cofiguration.OzonData;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.soap.*;
import java.io.StringWriter;

import static java.nio.charset.StandardCharsets.UTF_8;

@AllArgsConstructor
@Service
public class SoapService {

    private final ClientConfiguration config;

    public void callSoapWebService(Object requestBody) {
        try {
            String soapAction = OzonData.getSoapAction(requestBody);
            String soapEndpointUrl = OzonData.getEndpointUrl(requestBody);

//            System.out.println("SoapAction: " + soapAction);
//            System.out.println("EndPointUrl:" + soapEndpointUrl);

            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, requestBody), soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("\n\nResponse SOAP Message: \n");
            soapResponse.writeTo(System.out);

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }

    private SOAPMessage createSOAPRequest(String soapAction, Object requestBody) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, requestBody);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);
        headers.addHeader("Content-Type", config.getContentType());

        soapMessage.saveChanges();

        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);

        return soapMessage;
    }

    private void createSoapEnvelope(SOAPMessage soapMessage, Object requestBody) throws SOAPException, JAXBException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(config.getNamespace(), config.getNamespaceURI());

        SOAPBody soapBody = envelope.getBody();

        JAXBContext jaxbContext = JAXBContext.newInstance(requestBody.getClass());
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, UTF_8.toString());

        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(requestBody,soapBody);
    }

}
